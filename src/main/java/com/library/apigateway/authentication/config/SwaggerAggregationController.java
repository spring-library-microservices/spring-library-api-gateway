package com.library.apigateway.authentication.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import springfox.documentation.swagger.web.SwaggerResource;
import springfox.documentation.swagger.web.SwaggerResourcesProvider;

import java.util.ArrayList;
import java.util.List;

@Primary
@Configuration
public class SwaggerAggregationController implements SwaggerResourcesProvider {

    private final static String SWAGGER_VERSION = "2.0";

    @Override
    public List<SwaggerResource> get() {
        List<SwaggerResource> resources = new ArrayList<>();

        resources.add(swaggerResource("Borrowing Service", "/borrowing-service/v2/api-docs"));
        resources.add(swaggerResource("Books Api Service", "/books-api-service/v2/api-docs"));
        resources.add(swaggerResource("Gateway", "/library-api-gateway/v2/api-docs"));
        return resources;
    }


    private SwaggerResource swaggerResource(String name, String location) {

        SwaggerResource swaggerResource = new SwaggerResource();
        swaggerResource.setName(name);
        swaggerResource.setLocation(location);
        swaggerResource.setSwaggerVersion(SWAGGER_VERSION);

        return swaggerResource;
    }
}
