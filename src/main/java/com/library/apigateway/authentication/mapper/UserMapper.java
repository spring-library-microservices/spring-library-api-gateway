package com.library.apigateway.authentication.mapper;

import com.library.apigateway.authentication.dto.RegisterRequest;
import com.library.apigateway.authentication.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public User toDao(RegisterRequest registerRequest, String password, String cardNumber) {

        User user = new User();
        user.setName(registerRequest.getName());
        user.setSurname(registerRequest.getSurname());
        user.setEmail(registerRequest.getEmail());
        user.setAge(registerRequest.getAge());
        user.setPassword(password);
        user.setCardNumber(cardNumber);

        return user;
    }
}
