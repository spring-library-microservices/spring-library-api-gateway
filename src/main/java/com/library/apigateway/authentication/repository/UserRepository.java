package com.library.apigateway.authentication.repository;

import com.library.apigateway.authentication.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Transactional
    @Modifying
    @Query(value = "INSERT INTO users(card_number, name, surname, email, age,password) VALUES (:cardNumber,:name,:surname,:email,:age,:password);",nativeQuery = true)
    void saveUser(@Param("cardNumber") String cardNumber,
                    @Param("name") String name,
                    @Param("surname")String surname,
                    @Param("email") String email,
                    @Param("age") Integer age,
                    @Param("password") String password);


    @Query(value = "SELECT*FROM users where email=:email",nativeQuery = true)
    User getUserByEmail(@Param("email") String email);
}
