package com.library.apigateway.authentication.controller;


import com.library.apigateway.authentication.dto.LoginRequest;
import com.library.apigateway.authentication.dto.RegisterRequest;
import com.library.apigateway.authentication.service.UserAuthenticationService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {

    private final UserAuthenticationService userAuthenticationService;

    public AuthenticationController(UserAuthenticationService userAuthenticationService) {
        this.userAuthenticationService = userAuthenticationService;
    }

    @PostMapping(path = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        userAuthenticationService.registerUser(registerRequest);
        return new ResponseEntity<>("Registration Successful", HttpStatus.CREATED);
    }

    @PostMapping(path = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> login(@RequestBody LoginRequest loginRequest) {
        userAuthenticationService.loginUser(loginRequest);
        return new ResponseEntity<>("Login Successful", HttpStatus.OK);
    }
}
