package com.library.apigateway.authentication.exception;

public class AuthorizationException extends RuntimeException {

    public AuthorizationException(AuthErrorCode authErrorCode) {
        super(authErrorCode.message);
    }
}
