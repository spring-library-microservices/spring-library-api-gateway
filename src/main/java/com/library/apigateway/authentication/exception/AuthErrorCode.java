package com.library.apigateway.authentication.exception;

public enum AuthErrorCode {

    USER_NOT_FOUND("User with given email do not exists"),
    USER_EXISTS("User with given email already exists");

    final String message;

    AuthErrorCode(String message) {
        this.message = message;
    }

}
