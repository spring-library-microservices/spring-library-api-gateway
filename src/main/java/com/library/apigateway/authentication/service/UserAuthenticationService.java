package com.library.apigateway.authentication.service;

import com.library.apigateway.LibraryApiGatewayApplication;
import com.library.apigateway.authentication.dto.LoginRequest;
import com.library.apigateway.authentication.dto.RegisterRequest;
import com.library.apigateway.authentication.exception.AuthErrorCode;
import com.library.apigateway.authentication.exception.AuthorizationException;
import com.library.apigateway.authentication.mapper.UserMapper;
import com.library.apigateway.authentication.model.User;
import com.library.apigateway.authentication.repository.UserRepository;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserAuthenticationService {

    private final UserRepository userRepository;
    private final AuthenticationManager authenticationManager;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;

    public UserAuthenticationService(UserRepository userRepository, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, UserMapper userMapper) {
        this.userRepository = userRepository;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.userMapper = userMapper;
    }


    public void registerUser(RegisterRequest registerRequest) {

        String cardNumber = RandomStringUtils.random(10, true, true);

        User userToDao = userMapper.toDao(registerRequest,
                passwordEncoder.encode(registerRequest.getPassword()),
                cardNumber);

        User foundUser = userRepository.getUserByEmail(userToDao.getEmail());

        if (Objects.nonNull(foundUser)) {
            throw new AuthorizationException(AuthErrorCode.USER_EXISTS);
        }

        userRepository.saveUser(userToDao.getCardNumber(), userToDao.getName(), userToDao.getSurname(),
                userToDao.getEmail(), userToDao.getAge(), userToDao.getPassword());
    }


    public void loginUser(LoginRequest loginRequest) {

        Authentication userAuthentication = authenticationManager
                .authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(userAuthentication);
    }

}
