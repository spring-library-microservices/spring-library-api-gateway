package com.library.apigateway.authentication.service;


import com.library.apigateway.authentication.exception.AuthErrorCode;
import com.library.apigateway.authentication.exception.AuthorizationException;
import com.library.apigateway.authentication.model.User;
import com.library.apigateway.authentication.repository.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserRepository userRepository;

    public UserDetailsServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String email) {

        User validatedUser = userRepository.getUserByEmail(email);

        if (Objects.isNull(validatedUser)) {
            throw new AuthorizationException(AuthErrorCode.USER_NOT_FOUND);
        }

        return validatedUser;
    }
}
