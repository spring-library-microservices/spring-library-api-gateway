package com.library.apigateway;

import com.library.apigateway.authentication.model.User;
import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

@Component
public class ZuulLoggingFilter extends ZuulFilter {

    @Override
    public String filterType() {
        return "pre";
    }

    @Override
    public int filterOrder() {
        return 1;
    }

    @Override
    public boolean shouldFilter() {
        return true;
    }

    @Override
    public Object run() throws ZuulException {

        Object loggedUser = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        User user;

        if (loggedUser instanceof UserDetails) {
            user = (com.library.apigateway.authentication.model.User) loggedUser;
        } else {
            return null;
        }

        RequestContext context = RequestContext.getCurrentContext();
        context.addZuulRequestHeader("userId", String.valueOf(user.getId()));

        return null;
    }
}
